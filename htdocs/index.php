<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/styles.css"/>
    <script src="scripts/script.js"></script>
    <title>Assignment 07 -- Egri-Blogna, Jayden</title>
</head>

<body>
    <form method="get" action="" name="mainForm" onsubmit="return(valid());">
      <fieldset>
        <legend>CONTACT INFO:</legend>
        <p>
          <label>First Name: </label><br/>
          <span id="errorField1"></span><br/>
          <input type="text" id="firstName" name="firstName" />
        </p>

        <p>
          <label>Last Name: </label><br/>
          <span id="errorField2"></span><br/>
          <input type="text" id="lastName" />
        </p>

        <p>
          <label>Username: </label><br/>
          <span id="errorField3"></span><br/>
          <input type="text" id="username" />
        </p>

        <p>
          <label>Password: </label><br/>
          <span id="errorField4"></span><br/>
          <input type="text" id="password" />
        </p>

        <p>
          <label>Confirm Password: </label><br/>
          <span id="errorField5"></span><br/>
          <input type="text" id="checkPassword" />
        </p>

        <p>
          <label>E-mail: </label><br/>
          <span id="errorField6"></span><br/>
          <input type="text" id="email" />
        </p>

        <p>
          <label>Confirm E-mail: </label><br/>
          <span id="errorField7"></span><br/>
          <input type="text" id="checkEmail" />
        </p>

        <p>
          <label>Phone Number: [ex: (419) 123-6789]</label><br/>
          <span id="errorField8"></span><br/>
          <input type="text" id="phoneNumber" />
        </p>

        <p>
          <label>Street Address: </label><br/>
          <span id="errorField9"></span><br/>
          <input type="text" id="address" />
        </p>

        <p>
          <label>City: </label><br/>
          <span id="errorField10"></span><br/>
          <input type="text" id="city" />
        </p>

        <p>
          <label>State: </label><br/>
          <span id="errorField11"></span><br/>
          <input type="text" id="state" />
        </p>

        <p>
          <label>Postal Code: [ex: XXXXX]</label><br/>
          <span id="errorField12"></span><br/>
          <input type="text" id="postalCode" />
        </p>

        <p>
          <label>Date of Birth [MM/DD/YYYY]: </label><br/>
          <span id="errorField13"></span><br/>
          <input type="text" id="dob" />
        </p>

        <br>

        <div class="rectangle centered">
          <input type="submit" value="Submit" class="btn"> <input type="reset" value="Clear Form" class="btn">
        </div>

      </fieldset>
    </form>
  </body>
</html>
