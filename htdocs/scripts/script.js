function valid()
{
  var element = document.getElementById('firstName');
  if (element.value == "") {
      alert("Please enter a first name");
      document.getElementById('firstName').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-z,.'-]+$/gm, 'i');
      document.getElementById("errorField1").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid first name")
          document.getElementById('firstName').focus();
          document.getElementById('firstName').select();
          errorField1.style.color = "#ff0000";
          document.getElementById("errorField1").innerHTML = "* Please enter a valid first name.";

          return false;
      }
  }

  var element = document.getElementById('lastName');
  if (element.value == "") {
      alert("Please enter a last name");
      document.getElementById('lastName').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-z,.'-]+$/gm, 'i');
      document.getElementById("errorField2").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid last name")
          document.getElementById('lastName').focus();
          document.getElementById('lastName').select();
          errorField2.style.color = "#ff0000";
          document.getElementById("errorField2").innerHTML = "* Please enter a valid last name.";

          return false;
      }
  }

  var element = document.getElementById('username');
  if (element.value == "") {
      alert("Please enter a username");
      document.getElementById('username').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-z\d]{5,12}$/i, 'i');
      document.getElementById("errorField3").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid username")
          document.getElementById('username').focus();
          document.getElementById('username').select();
          errorField3.style.color = "#ff0000";
          document.getElementById("errorField3").innerHTML = "* Please enter a valid username.";

          return false;
      }
  }

  var element = document.getElementById('password');
  if (element.value == "") {
      alert("Please enter a password");
      document.getElementById('password').focus();
      return false;
  }
  else {
      var check = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{5,12}$/gm, 'i');
      document.getElementById("errorField4").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid password")
          document.getElementById('password').focus();
          document.getElementById('password').select();
          errorField4.style.color = "#ff6666";
          document.getElementById("errorField4").innerHTML = "Please enter a valid password.";

          return false;
      }
  }

  var element = document.getElementById('checkPassword');
  var element1 = document.getElementById('password');
  if (element.value == "") {
      alert("You must enter the password again");
      document.getElementById('checkPassword').focus();
      return false;
  }
  else {
      var check = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{5,12}$/gm, 'i');
      document.getElementById("errorField5").innerHTML = "";

      if ((check.test(element.value)==false) && element != element1) {
          alert("Not the same password")
          document.getElementById('checkPassword').focus();
          document.getElementById('checkPassword').select();
          errorField5.style.color = "#ff6666";
          document.getElementById("errorField5").innerHTML = "Password does not match.";

          return false;
      }
  }

  var element = document.getElementById('email');
  if (element.value == "") {
      alert("Please enter a email");
      document.getElementById('email').focus();
      return false;
  }
  else {
      var check = new RegExp(/^([a-z\d.-]+)@([a-z\d-]+).([a-z]{2,8})(.[a-z]{2,8})?$/gm, 'i');
      document.getElementById("errorField6").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid email")
          document.getElementById('email').focus();
          document.getElementById('email').select();
          errorField6.style.color = "#ff0000";
          document.getElementById("errorField6").innerHTML = "* Please enter a valid email.";

          return false;
      }
  }

  var element = document.getElementById('checkEmail');
  var element1 = document.getElementById('email');
  if (element.value == "") {
      alert("Please enter a email");
      document.getElementById('checkEmail').focus();
      return false;
  }
  else {
      var check = new RegExp(/^([a-z\d.-]+)@([a-z\d-]+).([a-z]{2,8})(.[a-z]{2,8})?$/gm, 'i');
      document.getElementById("errorField7").innerHTML = "";

      if ((check.test(element.value)==false) && element != element1){
          alert("Invalid email")
          document.getElementById('checkEmail').focus();
          document.getElementById('checkEmail').select();
          errorField7.style.color = "#ff0000";
          document.getElementById("errorField7").innerHTML = "* Please enter a valid email.";

          return false;
      }
  }

  var element = document.getElementById('phoneNumber');
  if (element.value == "") {
      alert("Please enter a phone number");
      document.getElementById('phoneNumber').focus();
      return false;
  }
  else {
      var check = new RegExp(/^(\([0-9]{3}\)\s*|[0-9]{3}\-)[0-9]{3}-[0-9]{4}$/gm, 'i');
      var check2 = new RegExp(/^\d{10}$/gm, 'i');
      document.getElementById("errorField8").innerHTML = "";

      if ((check.test(element.value)==false) && (check2.test(element.value)==false)){
          alert("Invalid phone number")
          document.getElementById('phoneNumber').focus();
          document.getElementById('phoneNumber').select();
          errorField8.style.color = "#ff0000";
          document.getElementById("errorField8").innerHTML = "* Please enter a valid phone number.";

          return false;
      }
  }

  var element = document.getElementById('address');
  if (element.value == "") {
      alert("Please enter a steet address");
      document.getElementById('address').focus();
      return false;
  }
  else {
      var check = new RegExp(/^\d{1,6}\040([A-Z]{1}[a-z]{1,}\040[A-Z]{1}[a-z]{1,}\040[A-Z]{1}[a-z]{1,})$/gm, 'i');
      document.getElementById("errorField9").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid street address")
          document.getElementById('address').focus();
          document.getElementById('address').select();
          errorField9.style.color = "#ff0000";
          document.getElementById("errorField9").innerHTML = "* Please enter a valid street address.";

          return false;
      }
  }

  var element = document.getElementById('city');
  if (element.value == "") {
      alert("Please enter a city");
      document.getElementById('city').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/gm, 'i');
      document.getElementById("errorField10").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid city")
          document.getElementById('city').focus();
          document.getElementById('city').select();
          errorField10.style.color = "#ff0000";
          document.getElementById("errorField10").innerHTML = "* Please enter a valid city.";

          return false;
      }
  }

  var element = document.getElementById('state');
  if (element.value == "") {
      alert("Please enter a state");
      document.getElementById('state').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/gm, 'i');
      document.getElementById("errorField11").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid state")
          document.getElementById('state').focus();
          document.getElementById('state').select();
          errorField11.style.color = "#ff0000";
          document.getElementById("errorField11").innerHTML = "* Please enter a valid state.";

          return false;
      }
  }

  var element = document.getElementById('postalCode');
  if (element.value == "") {
      alert("Please enter a postal code");
      document.getElementById('postalCode').focus();
      return false;
  }
  else {
      var check = new RegExp(/(^\d{5}$)|(^\d{5}-\d{3}$)/gm, 'i');
      document.getElementById("errorField12").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid postal code")
          document.getElementById('postalCode').focus();
          document.getElementById('postalCode').select();
          errorField12.style.color = "#ff0000";
          document.getElementById("errorField12").innerHTML = "* Please enter a valid postal code.";

          return false;
      }
  }

  var element = document.getElementById('dob');
  if (element.value == "") {
      alert("Please enter a date of birth");
      document.getElementById('dob').focus();
      return false;
  }
  else {
      var check = new RegExp(/^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/gm, 'i');
      document.getElementById("errorField13").innerHTML = "";

      if (check.test(element.value)==false){
          alert("Invalid date of birth")
          document.getElementById('dob').focus();
          document.getElementById('dob').select();
          errorField13.style.color = "#ff0000";
          document.getElementById("errorField13").innerHTML = "* Please enter a valid date of birth.";

          return false;
      }
  }

return true;
}
